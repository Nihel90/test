package guedri.nihel.mytestdpc.network;


import java.util.List;

import guedri.nihel.mytestdpc.model.Album;
import guedri.nihel.mytestdpc.model.Picture;
import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiRest {

    @GET("albums")
  //  Observable<Album> getAlbumsWithPictures ();
    Call<List<Album>> getAlbums ();


    @GET("photos")
    Call<List<Picture>> getPictures ();

}
