package guedri.nihel.mytestdpc.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    private static ApiRest INSTANCE;

    public static ApiRest getInstance() {
        synchronized (RetrofitService.class) {
            if (INSTANCE == null) {
                INSTANCE = new Retrofit.Builder()
                        .baseUrl("https://jsonplaceholder.typicode.com/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                        .create(ApiRest.class);
            }
        }
        return INSTANCE;
    }


}
