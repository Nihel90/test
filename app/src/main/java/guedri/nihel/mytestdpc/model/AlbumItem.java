package guedri.nihel.mytestdpc.model;

import java.util.List;

public class AlbumItem {
    private Album album;
    private List<Picture> pictures;

    public AlbumItem(Album album, List<Picture> pictures) {
        this.album = album;
        this.pictures = pictures;
    }

    public Album getAlbum() {
        return album;
    }

    public List<Picture> getPictures() {
        return pictures;
    }
}
