package guedri.nihel.mytestdpc.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import guedri.nihel.mytestdpc.R;
import guedri.nihel.mytestdpc.model.Album;
import guedri.nihel.mytestdpc.model.AlbumItem;
import guedri.nihel.mytestdpc.model.Picture;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {


    private Context context;
    ArrayList<AlbumItem> albumItemArrayList;



    public AlbumAdapter(Context context, ArrayList<AlbumItem> albumItemArrayList) {
        this.context = context;
        this.albumItemArrayList = albumItemArrayList;
    }




    @NonNull
    @Override
    public AlbumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.ViewHolder holder, int position) {

        AlbumItem albumItem =albumItemArrayList.get(position);

        holder.txtAlbumId.setText(" Album title "+albumItem.getAlbum().getId().toString());
        List<Picture> pics = albumItem.getPictures();
        if (pics.get(0) != null) {
            Picasso.get().load(pics.get(0).thumbnailUrl).into(holder.picture1);
        }
        if (pics.get(1) != null) {
            Picasso.get().load(pics.get(1).thumbnailUrl).into(holder.picture2);
        }
        if (pics.get(2) != null) {
            Picasso.get().load(pics.get(2).thumbnailUrl).into(holder.picture3);
        }
    }



    @Override
    public int getItemCount()
    {
        return albumItemArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtAlbumId;
        private ImageView picture1;
        private ImageView picture2;
        private ImageView picture3;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtAlbumId = (TextView) itemView.findViewById(R.id.txt_albumId);
            picture1 = (ImageView) itemView.findViewById(R.id.imageView1);
            picture2 = (ImageView) itemView.findViewById(R.id.imageView2);
            picture3 = (ImageView) itemView.findViewById(R.id.imageView3);


        }
    }



}
