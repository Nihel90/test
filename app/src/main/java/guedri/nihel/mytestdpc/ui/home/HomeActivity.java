package guedri.nihel.mytestdpc.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import guedri.nihel.mytestdpc.R;
import guedri.nihel.mytestdpc.model.Album;
import guedri.nihel.mytestdpc.model.AlbumItem;

public class HomeActivity extends AppCompatActivity {


    HomeViewModel viewModel;
    //  FragmentFormationsBinding binding;



    private RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;
    private AlbumAdapter adapter;
    private ArrayList<AlbumItem> albumItemArrayList = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        //    viewModel.setActivity(this);

        //   binding = DataBindingUtil.inflate(inflater, R.layout.fragment_formations, container, false);
        //   View view = binding.getRoot();
        //   binding.setLifecycleOwner(this);
        //   binding.setViewModel(viewModel);



        recyclerView = (RecyclerView)findViewById(R.id.list_album);


        // use a linear layout manager
        //   layoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // my_recycler_view_horizontal.setHasFixedSize(true);
        //my_recycler_view_vertical.setHasFixedSize(true);


        // adapter
        adapter = new AlbumAdapter(this, albumItemArrayList );
        recyclerView.setAdapter(adapter);


        getAlbums();

    }


    private void getAlbums() {
        viewModel.getAlbumLiveData().observe(this, albums -> {

            if(albums !=null){

                //    progress_circular_movie_article.setVisibility(View.GONE);
                List<AlbumItem> albumItemList = albums;
                albumItemArrayList.addAll(albumItemList);
                adapter.notifyDataSetChanged();
            }

        });
        viewModel.getAlbums(this);

    }





}
