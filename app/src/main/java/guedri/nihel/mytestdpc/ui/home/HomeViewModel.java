package guedri.nihel.mytestdpc.ui.home;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import java.util.List;

import guedri.nihel.mytestdpc.model.AlbumItem;
import guedri.nihel.mytestdpc.repository.HomeRepository;
import io.paperdb.Paper;

public class HomeViewModel extends ViewModel {


    private HomeRepository repository;
    private MutableLiveData<List<AlbumItem>> albumLiveData = new MutableLiveData<>();

    public HomeViewModel() {
        repository = new HomeRepository();

    }

    public LiveData<List<AlbumItem>> getAlbumLiveData() {
        return albumLiveData;
    }

    public void getAlbums(HomeActivity activity) {
        List<AlbumItem> data = Paper.book().read("data");
        if (null != data) {
            albumLiveData.setValue(data);
        } else {
            repository.dataAlbumItem.observe(activity, new Observer<List<AlbumItem>>() {
                @Override
                public void onChanged(List<AlbumItem> albumItems) {
                    albumLiveData.setValue(albumItems);
                }
            });
            repository.getAlbumsWithPictures();
        }
    }
}
