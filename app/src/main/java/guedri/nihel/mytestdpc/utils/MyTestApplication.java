package guedri.nihel.mytestdpc.utils;

import android.app.Application;

import io.paperdb.Paper;

public class MyTestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);
    }
}
