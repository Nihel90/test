package guedri.nihel.mytestdpc.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import guedri.nihel.mytestdpc.model.Album;
import guedri.nihel.mytestdpc.model.AlbumItem;
import guedri.nihel.mytestdpc.model.Picture;
import guedri.nihel.mytestdpc.network.ApiRest;
import guedri.nihel.mytestdpc.network.RetrofitService;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepository {

    public MutableLiveData<List<AlbumItem>> dataAlbumItem = new MutableLiveData<>();
    private ApiRest apiRest;

    public HomeRepository() {
        apiRest = RetrofitService.getInstance();
    }

    public void getAlbumsWithPictures() {
        Call<List<Album>> call = apiRest.getAlbums();
        Call<List<Picture>> callPictures = apiRest.getPictures();
        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                List<Album> albums = response.body();
                if (albums != null) {
                    callPictures.enqueue(new Callback<List<Picture>>() {
                        @Override
                        public void onResponse(Call<List<Picture>> call, Response<List<Picture>> response) {
                            List<Picture> pictures = response.body();
                            if (pictures != null) {
                                List<AlbumItem> items = new ArrayList<>();
                                for (Album album : albums) {
                                    List<Picture> pics = getPictureByAlbum(pictures, album.id);
                                    items.add(new AlbumItem(album, pics));
                                }
                                Paper.book().write("data", items);
                                dataAlbumItem.postValue(items);
                            }
                        }
                        @Override
                        public void onFailure(Call<List<Picture>> call, Throwable t) {
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
            }
        });
    }

    private List<Picture> getPictureByAlbum(List<Picture> pictures, Integer id) {
        List<Picture> pics = new ArrayList<>();
        for (Picture picture : pictures) {
            if (id == picture.albumId) {
                pics.add(picture);
            }
        }
        Collections.sort(pics);
        if (pics.size() > 3) {
            return pics.subList(0, 3);
        }
        return pics;
    }
}
